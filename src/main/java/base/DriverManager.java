package base;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebDriver;


public class DriverManager {

    private static RemoteWebDriver driver;




    public  static WebDriver init(){
       try {
//            driver = new FirefoxDriver();
//            System.setProperty("webdriver.gecko.driver", "geckodriver");
           driver = new ChromeDriver();
           System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
//           ChromeOptions options = new ChromeOptions();
//           options.addArguments("--no-sandbox");
//           options.addArguments("--disable-dev-shm-usage");

           driver.manage().window().maximize();
           driver.get("https://hotels.com");

       }catch (Exception e){
           e.printStackTrace();
       }
        return driver;
    }

    public  static WebDriver getDriver(){
           return driver;
    }

    public static void tearDown(){
        if(driver!=null){
            driver.quit();
        }

    }
}
