package pages;

import org.openqa.selenium.By;

public class GoogleSignInPage {
    By enterEmail = By.xpath("//*[@id='identifierId']");
    By nextButton = By.xpath("//*[@class='RveJvd snByac']");
    public By getNextButton() { return nextButton; }
    public By getEnterEmail() { return enterEmail; }
}
