package pages;

import org.openqa.selenium.By;

public class LoginPage {
    private By signInButton = By.xpath("//button[@type='submit']");
    private By signInWithGoogle = By.xpath("//*[@data-label='Google']");

    public By getSignInWithGoogle() {
        return signInWithGoogle;
    }

    public By getSignInButton() {
        return signInButton;
    }
}
