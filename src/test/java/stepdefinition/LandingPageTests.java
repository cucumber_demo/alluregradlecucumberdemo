package stepdefinition;

import base.DriverManager;
import base.TestBase;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import org.testng.Assert;
import pages.GoogleSignInPage;
import pages.LandingPage;
import pages.LoginPage;

public class LandingPageTests extends DriverManager {



    TestBase base = new TestBase();
    LandingPage landingPage = new LandingPage();
    LoginPage loginPage = new LoginPage();
    GoogleSignInPage getSingInWithGoogle = new GoogleSignInPage();

    @Given("User in landing page of hotels.com")
    public void user_in_landing_page_of_hotels_com() {
       base.clickOn(landingPage.getPopUpCancelBtn());
       Assert.assertEquals( base.getTitleOfThePage(),"Hotels.com - Deals & Discounts for Hotel Reservations from Luxury Hotels to Budget Accommodations");
    }

    @Then("User clicks on sign in button")
    public void user_clicks_on_sign_in_button() {

        base.clickOn(landingPage.getSignInMenuOption());
    }

    @Then("User validates sign in page title")
    public void user_validates_sign_in_page_title() {

        Assert.assertEquals( base.getTitleOfThePage(),"Hotels.com - Deals & Discounts for Hotel Reservations from Luxury Hotels to Budget Accommodations");
    }

    @Then("^User validates \"([^\"]*)\" of the sign in button$")
    public void user_validates_of_the_sign_in_button(String arg1)  {
        Assert.assertEquals( base.getTextOfElement(loginPage.getSignInButton()),arg1);
    }

    @Then("User clicks sign in with Google button")
    public void user_clicks_sign_in_with_Google_button() throws Throwable {
        base.clickOn(loginPage.getSignInWithGoogle());
        base.switchToNewWindow();

    }
    @Then("User types email address")
    public void user_types_email_address() throws Throwable {
        Thread.sleep(5000);
        base.sendText(getSingInWithGoogle.getEnterEmail(), "ayzik7737@gmail.com");
        base.clickOn(getSingInWithGoogle.getNextButton());
    }

}
