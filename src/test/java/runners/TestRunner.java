package runners;
import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;
@CucumberOptions(
        features = {"classpath:features"}
        ,glue={"stepdefinition"},

//        strict = true,
//        dryRun = true,
        tags = {" @Run"},
        plugin = {"io.qameta.allure.cucumberjvm.AllureCucumberJvm"}
//        plugin = {
//                "pretty",
//                "html:target/cucumber-html-default","json:target/cucumber-report.json",
//                "junit:target/cucumber-report.xml" }
)
public class TestRunner extends AbstractTestNGCucumberTests {




}
